﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Complete;
//personality types
public enum Personality
{
    Chase,
    Flee,
    Patrol,
    Sensing
}
//loopmode for patrol personality
public enum LoopMode
{
    Loop,
    PingPong
}

public class EnemyController : MonoBehaviour
{
    public Personality TankPersonality;
    //obsticle avoidence 
    private bool avoidingObstacle = false;
    private TankMovement movement;
    //makes sure the player is being targeted 
    public Transform Target;
    //patrol points
    public List<Transform> PatrolPoints;
    public int currentPoint;
    public float closeEnoughDistance;
    public LoopMode PatrolLoop;
    private bool PingPongDirection;
    //obsticale avoidence 
    public LayerMask SightObstacles;
    //senses
    public float SightRange;
    public float HearingRange;
    //AI can shoot
    private TankShooting shooter;

    // Start is called before the first frame update
    void Start()
    {
        //movement
        movement = this.GetComponent<TankMovement>();
        //shooting
        shooter = this.GetComponent<TankShooting>();

    }
    //shooting 
    public void Shoot()
    {
        shooter.Fire();
    }
    //chase
    public void Chase()
    {
        Shoot();
        movement.m_MovementInputValue = 1f;
        movement.m_TurnInputValue = DirectionToTarget();
    }
    //flee
    public void Flee()
    {
        Shoot();
        movement.m_MovementInputValue = 1f;
        movement.m_TurnInputValue = -DirectionToTarget();
    }
    //patrol
    public void Patrol()
    {
        Shoot();
        movement.m_MovementInputValue = 1f;
        movement.m_TurnInputValue = DirectionToPoint();
        if (Vector3.Distance(PatrolPoints[currentPoint].position, transform.position) <= closeEnoughDistance)
        {
            //so the AI always patrols 
            if (PatrolLoop == LoopMode.Loop)
            {
                Loop();
            }
            else if (PatrolLoop == LoopMode.PingPong)
            {
                PingPong();
            }
        }
    }
    public float DirectionToTarget()
    {
        //makes sure patrolling AI pays attention to the player 
        float angle = Vector3.SignedAngle(transform.forward, Target.position - transform.position, Vector3.up);
        if (angle < 0f)
        {
            return -1f;
        }
        else
        {
            return 1f;
        }
    }
    public float DirectionToPoint()
    {
        //If the player is to far away patroling AI continues to patrol
        float angle = Vector3.SignedAngle(transform.forward, PatrolPoints[currentPoint].position - transform.position, Vector3.up);
        if (angle < 0f)
        {
            return -1f;
        }
        else
        {
            return 1f;
        }
    }
    public void Loop()
    {
        currentPoint = currentPoint + 1;
        if (currentPoint == PatrolPoints.Count)
        {
            currentPoint = 0;
        }
    }
    public void PingPong()
    {
        if (PingPongDirection == false)
        {
            currentPoint = currentPoint + 1;
            if (currentPoint == PatrolPoints.Count)
            {
                currentPoint = PatrolPoints.Count - 1;
                PingPongDirection = true;
            }
        }
        else
        {
            currentPoint = currentPoint - 1;
            if (currentPoint < 0)
            {
                currentPoint = 0;
                PingPongDirection = false;
            }
        }
    }
    public void Sensing()
    {
        //sensing 
        if (SeeingPlayer())
        {
            Shoot();
            movement.m_MovementInputValue = 0.8f;
            movement.m_TurnInputValue = DirectionToTarget();
        }
        else if (HearingPlayer())
        {
            movement.m_MovementInputValue = 0f;
            movement.m_TurnInputValue = DirectionToTarget();
        }
        else
        {
            movement.m_MovementInputValue = 0f;
            movement.m_TurnInputValue = 0f;
        }
    }
    public bool SeeingPlayer()
    {
        //seeing player
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, SightRange, SightObstacles))
        {
            if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Players"))
            {
                return true;
            }
        }
        return false;
    }
    public bool HearingPlayer()
    {
        //hearing player
        if (Vector3.Distance(transform.position, Target.position) <= HearingRange)
        {
            return true;
        }
        return false;
    }
    // Update is called once per frame
    void Update()
    {
        //movement.m_MovementInputValue = 0f;
        //movement.m_TurnInputValue = 0f;
        if (avoidingObstacle == true)
        {

        }
        else
        {
            //personalities 
            if (TankPersonality == Personality.Flee)
            {
                Flee();
            }
            if (TankPersonality == Personality.Chase)
            {
                Chase();
            }
            if (TankPersonality == Personality.Patrol)
            {
                Patrol();
            }
            if (TankPersonality == Personality.Sensing)
            {
                Sensing();
            }
        }
    }
}

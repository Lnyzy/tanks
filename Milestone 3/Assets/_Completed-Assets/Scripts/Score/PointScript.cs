﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointScript : MonoBehaviour
{
    public int points = 0;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnGUI()
    {
        // My label for player score
        GUI.color = Color.white;
        GUI.Label(new Rect(10, 10, 100, 20), "Score : " + points);
    }
}